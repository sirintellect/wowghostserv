const http = require('http')
const fs = require('fs')
var url = require('url')
const port = 3000

let config = {
  enable: 1, 
  durationOfGame: 600,
  speedOfGhosts: 1,
  massOfGhost: 0.25,
  durationOfGhostStop: 5,
  durationOfGhostDeath: 30,
  increaseOfSpeed: 5,
  numberOfGhosts: 8, 
  startTime: 0
}

const handleIndex = (request, response) => {
  fs.readFile('index.html', function(err, data) { 
    if (err){
      response.statusCode = 500
      response.end(`Error getting the file: ${err}.`)
    } else {
      response.setHeader('Content-type', 'text/html' )
      response.end(data)
    }
  })
}

const handleRead = (request, response) => { 
  response.setHeader('Content-type', 'application/json')
  response.end(JSON.stringify(config));
}

const handleWrite = (request, response) => { 
  var body = [];

  request.on('data', (chunk) => {
    body.push(chunk);
  }).on('end', () => {
    body = Buffer.concat(body).toString(); 
    body = JSON.parse('{"' + body.replace(/&/g, '","').replace(/=/g,'":"') + '"}', function(key, value) { return key===""?value:decodeURIComponent(value) })
    config = {
      ...config, 
      enable: body.enable === 'on' ? true : false,
      durationOfGame: parseInt(body.durationOfGame),
      speedOfGhosts: parseInt(body.speedOfGhosts),
      numberOfGhosts: parseInt(body.numberOfGhosts),
      increaseOfSpeed: parseFloat(body.increaseOfSpeed),
      durationOfGhostStop: parseInt(body.durationOfGhostStop),      
      durationOfGhostDeath: parseInt(body.durationOfGhostDeath),
      massOfGhost: parseFloat(body.massOfGhost),
      startTime: Math.floor(Date.now() / 1000)
    }

    handleIndex(request, response)    
  });

}

const handleRequest = (request, response) => { 
  var q = url.parse(request.url, true) 

  switch (q.pathname) {
    case '/config': 
      if (request.method === 'POST') {
        handleWrite(request, response) 
        return
      }

      handleRead(request, response) 
      break

    default:
      handleIndex(request, response)    
      break
  } 
}

const server = http.createServer(handleRequest)

server.listen(port, (err) => {
  if (err) {
    return console.log('Error: ', err)
  }

  console.log(`server is listening on ${port}`)
})
